def is_isogram(string):
    letters = [char for char in string.replace('-', '').replace(' ', '').lower()]
    return len(letters) == len(set(letters))
