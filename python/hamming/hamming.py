def distance(strand_a, strand_b):
    if len(strand_b) - len(strand_a) != 0:
        raise ValueError("Err")
    return sum([char[0] != char[1] for char in zip(strand_a, strand_b)])
