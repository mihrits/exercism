def score(word):
    points = {}
    for letter in ["A", "E", "I", "O", "U", "L", "N", "R", "S", "T"]:
        points[letter] = 1
    for letter in ["D", "G"]:
        points[letter] = 2
    for letter in ["B", "C", "M", "P"]:
        points[letter] = 3
    for letter in ["F", "H", "V", "W", "Y"]:
        points[letter] = 4
    for letter in ["K"]:
        points[letter] = 5
    for letter in ["J", "X"]:
        points[letter] = 8
    for letter in ["Q", "Z"]:
        points[letter] = 10

    lettercount = {}
    for letter in set(list(word.upper())):
        lettercount[letter] = word.upper().count(letter)

    score = 0
    for letter in lettercount.keys():
        score += lettercount[letter] * points[letter]

    return score
