def recite(start_verse, end_verse):
    return [mytruelove(day) + gifts(day) for day in range(start_verse, end_verse+1)]


def mytruelove(daynr):
    numbers = [
        "first",
        "second",
        "third",
        "fourth",
        "fifth",
        "sixth",
        "seventh",
        "eighth",
        "ninth",
        "tenth",
        "eleventh",
        "twelfth"
    ]
    return "On the " + numbers[daynr - 1] + " day of Christmas my true love gave to me: "


def gifts(daynr):
    verses = [
        "twelve Drummers Drumming, ",
        "eleven Pipers Piping, ",
        "ten Lords-a-Leaping, ",
        "nine Ladies Dancing, ",
        "eight Maids-a-Milking, ",
        "seven Swans-a-Swimming, ",
        "six Geese-a-Laying, ",
        "five Gold Rings, ",
        "four Calling Birds, ",
        "three French Hens, ",
        "two Turtle Doves, ",
        "and a Partridge in a Pear Tree."
    ]
    if daynr == 1:
        return "a Partridge in a Pear Tree."
    else:
        gifts = ""
        while daynr > 0:
            gifts += verses[-daynr]
            daynr -= 1
        return gifts
