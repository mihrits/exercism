class School:
    def __init__(self):
        self.enrolled = {}  # dict -> grade: [students]

    def add_student(self, name, grade):
        if grade not in self.enrolled.keys():
            self.enrolled[grade] = [name]
        else:
            self.enrolled[grade].append(name)

    def grade(self, grade_number):
        if grade_number not in self.enrolled.keys():
            return []
        return sorted(self.enrolled[grade_number])

    def roster(self):
        students = []
        for grade_number in sorted(list(self.enrolled.keys())):
            students += sorted(self.grade(grade_number))
        return students
