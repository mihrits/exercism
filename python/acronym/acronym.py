def abbreviate(words):
    w = [w.strip(" '_").upper()[0] for w in words.replace('-', ' ').split()]
    return ''.join(w)
