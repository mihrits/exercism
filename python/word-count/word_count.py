def count_words(sentence):
    words = [word.strip(",.!?'&@$%^:;") for word in
             sentence.lower().replace("_", " ").replace(",", " ").split()]
    while "" in words:
        words.remove("")
    wordcounts = {}
    for word in set(words):
        wordcounts[word] = words.count(word)
    return wordcounts
