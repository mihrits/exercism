class Luhn:
    def __init__(self, card_num):
        if card_num.replace(" ", "").isdigit():
            if len(card_num.replace(" ", "")) > 1:
                self.cnum = list(map(int, card_num.replace(" ", "")))
            else:
                self.cnum = [1]  # for self.valid to return False
        else:
            self.cnum = [1]  # for self.valid to return False

    def valid(self):
        a = list(reversed([sum(map(int, list(str(2*i)))) for i in self.cnum[-2::-2]]))
        b = list(reversed(self.cnum[-1::-2]))
        return (sum(a) + sum(b)) % 10 == 0
