defstudents = ["Alice", "Bob", "Charlie", "David", "Eve", "Fred",
               "Ginny", "Harriet", "Ileana", "Joseph", "Kincaid", "Larry"]

plantnames = {"G": "Grass", "C": "Clover", "R": "Radishes", "V": "Violets"}


class Garden:
    def __init__(self, diagram, students=defstudents):
        self.row1 = list(diagram.split('\n')[0])
        self.row2 = list(diagram.split('\n')[1])
        self.students = {student: num for student, num
                         in zip(sorted(students), range(len(students)))}

    def plants(self, student):
        num = self.students[student] * 2
        plants_short = self.row1[num:num+2] + self.row2[num:num+2]
        return [plantnames[plant] for plant in plants_short]
